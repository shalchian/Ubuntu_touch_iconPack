<img src="preview/icon_default.png" width="192" align="right" hspace="20" />

Ubuntu touch icon pack
======

![API](https://img.shields.io/badge/API-16%2B-34bf49.svg)


Ubuntu touch icon pack for android launchers.


<a target="_blank" href="https://play.google.com/store/apps/details?id=co.aseman.ubuntu_touch_icons">
<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" width="200"/>
</a>

# Previews

<p align="center">
<img src="preview/preview_1.png" height="350"/>
<img src="preview/preview_2.png" height="350"/>
<img src="preview/preview_3.png" height="350"/>


---

# Developed by

### [Armin Shalchian](https://github.com/Rminsh)


## Special thanks 🙌

- [Bardia Daneshvar](https://github.com/realbardia) 🛠
- [BluePrint Team](https://github.com/jahirfiquitiva/Blueprint) for Speeding up our icon pack development 📱
- [Suru icon theme](https://github.com/snwh/suru-icon-theme) for default icons 🎨


---

# License

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Ubuntu touch icon pack</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/Rminsh" property="cc:attributionName" rel="cc:attributionURL">Armin Shalchian</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/Rminsh/Ubuntu_touch_iconPack" rel="dct:source">https://github.com/Rminsh/Ubuntu_touch_iconPack</a>.